# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( LArG4ShowerLibSvc )

# External dependencies:
find_package( Geant4 )
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread )

atlas_add_library( LArG4ShowerLibSvcLib
                   LArG4ShowerLibSvc/*.h
                   INTERFACE
                   PUBLIC_HEADERS LArG4ShowerLibSvc
                   LINK_LIBRARIES GaudiKernel LArG4Code AthenaBaseComps )
set_target_properties( LArG4ShowerLibSvcLib PROPERTIES INTERPROCEDURAL_OPTIMIZATION ${ATLAS_GEANT4_USE_LTO} )

# Component(s) in the package:
atlas_add_library( LArG4ShowerLibSvc
                   src/*.cxx
                   src/components/*.cxx
                   OBJECT
                   NO_PUBLIC_HEADERS
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${GEANT4_INCLUDE_DIRS}
                   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} ${GEANT4_LIBRARIES} LArG4ShowerLibSvcLib AthenaKernel LArG4ShowerLib PathResolver )
set_target_properties( LArG4ShowerLibSvc PROPERTIES INTERPROCEDURAL_OPTIMIZATION ${ATLAS_GEANT4_USE_LTO} )
