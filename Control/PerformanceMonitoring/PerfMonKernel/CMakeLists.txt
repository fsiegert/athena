# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( PerfMonKernel )

# Component(s) in the package:
atlas_add_library( PerfMonKernel
                   src/*.cxx
                   PUBLIC_HEADERS PerfMonKernel
                   LINK_LIBRARIES GaudiKernel )
