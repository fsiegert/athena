#!/bin/bash
# art-description: Standard test for 2024 data
# art-type: grid
# art-include: main/Athena
# art-include: 24.0/Athena
# art-output: physval*.root
# art-output: *.xml
# art-output: dcube*
# art-html: dcube_shifter_last

# Fix ordering of output in logfile
exec 2>&1
run() { (set -x; exec "$@") }

relname="r24.0.67"

artdata=/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art
inputBS=${artdata}/RecJobTransformTests/data24_13p6TeV.00484909.physics_Main.daq.RAW/data24_13p6TeV.00484909.physics_Main.daq.RAW._lb0098._SFO-16._0001.data
# Ref to be updated when available
dcubeRef=${artdata}/InDetPhysValMonitoring/ReferenceHistograms/${relname}/physval_data24_13p6TeV_1000evt.root 
lastref_dir=last_results

script=test_data_reco.sh

conditions="CONDBR2-BLKPA-2024-04"
geotag="ATLAS-R3S-2021-03-02-00"

echo "Executing script ${script}"
echo " "
"$script" ${inputBS} ${dcubeRef} ${lastref_dir} ${conditions} ${geotag}
